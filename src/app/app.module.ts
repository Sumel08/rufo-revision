import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PanelComponent } from './panel/panel.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AlbumComponent } from './album/album.component';
import { MatIconModule, MatButtonModule, MatListModule, MatDialogModule} from '@angular/material';
// import { ImageComponent } from './image/image.component';
// import { ImageService } from './image/image.service';
import { HomeComponent } from './home/home.component';
import { MapComponent } from './map/map.component';
import { HttpClientModule } from '@angular/common/http';
import { AgmCoreModule } from '@agm/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MapFormComponent} from './album/form/form.component';
import { BlogComponent } from './blog/blog.component';
// import { Subject } from 'rxjs';

@NgModule({
  declarations: [
    AppComponent,
    PanelComponent,
    AlbumComponent,
    // ImageComponent,
    HomeComponent,
    MapComponent,
    MapFormComponent,
    BlogComponent,
    // DialogImage                                                                                                                                                                      
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatIconModule,
    MatListModule,
    MatDialogModule,
    ReactiveFormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDjjmy7Fpp650ZY2IE0E5w1f_Cwk2dNKLo'
    })],
  providers: [
    // ImageService
  ],
  bootstrap: [AppComponent],
  exports: [MatButtonModule,
    MatIconModule, MatListModule],
  // entryComponents: [DialogImage],
  entryComponents: [AlbumComponent]
})
export class AppModule { }
