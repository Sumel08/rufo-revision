import {Component, OnInit} from '@angular/core';
// import {MatDialog, MatDialogConfig, MatCard, MatCardHeader} from "@angular/material";
import {Observable, of} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {map, share} from 'rxjs/internal/operators';


class Image {
  id: number;
  url: string;
  thumbnailUrl: string;
}

@Component({
  selector: 'app-album',
  templateUrl: './album.component.html',
  styleUrls: ['./album.component.css']
})
export class AlbumComponent implements OnInit {
  private _albumObservable$: Observable<Image[]>;
  albumObservable: Observable<Image[]>;

  private _page = 0;
  private _pageSize = 10;

  // theImages = [];
  constructor(private httpClient: HttpClient) {
  }

  ngOnInit() {
    this.httpClient.get<Image[]>('https://jsonplaceholder.typicode.com/photos').subscribe(value => {
      this._albumObservable$ = of(value).pipe(share());
      this.changePage(true);
    });
  }

  changePage(next: boolean) {
    this._page += next ? 1 : -1;
    this._page = this._page <= 0 ? 1 : this._page;
    this.albumObservable = this._albumObservable$
      .pipe(
        map((data: Image[]) => data.slice(this._page * this._pageSize, this._pageSize * (this._page + 1)))
      );
  }

  // openDialog() {
  //   const dialogConfig = new MatDialogConfig();
  //   dialogConfig.disableClose = true;
  //   dialogConfig.autoFocus = true;
  //   this.dialog.open(DialogImage, dialogConfig);
  // }
}
