import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

class Post {
  userId: number;
  id: number;
  title: string;
  body: string;
}

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css']
})
export class BlogComponent implements OnInit {
  blogObservable : Observable<Post[]>;
  constructor( private httpClient: HttpClient) { }

  ngOnInit() {
    this.blogObservable = this.httpClient.get<Post[]>("https://jsonplaceholder.typicode.com/posts");
  }
}
