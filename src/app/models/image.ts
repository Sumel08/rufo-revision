export class Image {
  constructor(public id: string, public url: string, public thumbnailUrl: string) {}
}
